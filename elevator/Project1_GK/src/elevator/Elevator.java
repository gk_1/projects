package elevator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * 
 * Filled Elevator class, passes TestElevator_DP2.
 * 
 * @author Greg Kline
 * 
 * Project Due: October 13th, 2017
 * 
 *         Elevator class to simulate the operation of an elevator. Details
 *         associated with opening and closing of the elevator doors, entry and
 *         exit of people to and from the elevator, the number of people in,
 *         entering or leaving the elevator, and the timing of the movement of
 *         the elevator are all "abstracted" out of the problem, encompassing
 *         all of these actions into a single "move()" operation.
 * 
 * @author Daniel Plante
 *
 */
public class Elevator implements ElevatorOperations {
	private int myNumberOfFloors;
	private int currentFloor = 1;
	private boolean[] inButtons;
	private int direction = -1;
	private boolean up = false, down = false, hasDown = false;
	private ArrayList<Integer> sequence = new ArrayList<Integer>();

	/**
	 * Default constructor setting the number of floors for the elevator to five (5)
	 */
	public Elevator() {
		this(5);
	}

	/**
	 * Constructor setting the number of floors
	 * 
	 * @param numFloors
	 *            total number of floors
	 */
	public Elevator(int numFloors) {
		myNumberOfFloors = numFloors;
		inButtons = new boolean[numFloors];
	}

	/**
	 * unused part of old move() method
	 * 
	 * @return
	 */
	public boolean checkHasUp() {
		for (int i = currentFloor; i < myNumberOfFloors + 1; i++) {
			if (inButtons[i - 1] == true) {
				return true;
			}
		}
		return false;
	}

	/**
	 * unused part of old move() method
	 * 
	 * @return
	 */
	public boolean checkHasDown() {
		for (int i = currentFloor; i > 0; i--) {
			if (inButtons[i - 1] == true) {
				return true;
			}
		}
		return false;
	}

	/**
	 * simulates someone pressing the outside up button for an elevator
	 * 
	 * @param floor
	 *            the floor that the elevator has to go to
	 * 
	 * @return
	 */
	public boolean pushUp(int floor) {
		if (floor < myNumberOfFloors && floor != 0) {
			if (floor > currentFloor) {
				direction = 1;
				up = true;
			} else if (floor < currentFloor) {
				direction = 2;
				down = true;
			} else {
				direction = -1;
			}
			setFloor(floor);
			return true;
		}
		return false;
	}

	/**
	 * simulates someone pressing the outside down button for an elevator
	 * 
	 * @param floor
	 *            the floor that the elevator has to go to
	 * 
	 * @return
	 */
	public boolean pushDown(int floor) {
		if (floor > 1 && floor < myNumberOfFloors + 1) {
			down = true;
			if (direction == -1) {
				System.out.println("Down was pushed");
				direction = 2;
			}
			setFloor(floor);
			return true;
		}
		return false;
	}

	/**
	 * simulates someone pressing one of the inside numbered buttons for an elevator
	 * 
	 * @param floor
	 *            the floor someone is attempting to arrive at
	 * 
	 * @return
	 */
	public boolean pushIn(int floor) {

		if (up) {
			setFloor(floor);
			return true;
		}
		if (down) {
			setFloor(floor);
			return true;
		}
		return false;
	}

	/*
	 * //OLD MOVE public int move() { int lastRememberedFloor = currentFloor; if
	 * (getDirection() == UP) { for (int i = lastRememberedFloor; i <
	 * myNumberOfFloors; i++) { if (inButtons[i - 1] == true) { inButtons[i - 1] =
	 * false; currentFloor = i; break; } } }
	 * 
	 * if (getDirection() == DOWN) { for (int i = lastRememberedFloor; i > 0; i--) {
	 * if (inButtons[i - 1] == true) { inButtons[i - 1] = false; currentFloor = i;
	 * break; }
	 * 
	 * } }
	 * 
	 * if (up == true && checkHasUp() == false) { direction = 1; up = false; } else
	 * if (down == true && checkHasDown() == false) { direction = 2; down = false; }
	 * else if (checkHasUp() == false && checkHasDown() == false) { direction = -1;
	 * up = false; down = false; } System.out.println("The current floor is: " +
	 * currentFloor); System.out.println("First flagged floor: " + sequence.get(0));
	 * System.out.println("Second flagged floor: " + sequence.get(1)); return
	 * currentFloor; }
	 */

	/**
	 * Handles elevator movement between floors
	 * 
	 * @return currentFloor
	 */
	public int move() {

		if (sequence.isEmpty() == true) {
			return currentFloor;
		}

		if (sequence.get(0) == currentFloor) {
			currentFloor = sequence.get(0);
			sequence.remove(0);
			return currentFloor;
		}

		if (sequence.isEmpty() != true) {

			if (sequence.get(0) > currentFloor) {
				// Sort the array list in ascending order if the elevator will be heading up
				Collections.sort(sequence);
				System.out.println(sequence.toString());
			} else if (sequence.get(0) < currentFloor && sequence.get(0) != currentFloor) {
				// Sort the array list in descending order if the elevator will be heading down
				Collections.sort(sequence, Collections.reverseOrder());
				System.out.println(sequence.toString());
			} else if (sequence.get(0) == currentFloor) {

			}
			// Remove the floor that was just visited, this makes the next floor now at
			// index 0
			System.out.println("Current Floor: " + sequence.get(0));
			currentFloor = sequence.get(0);
			sequence.remove(0);
			return currentFloor;
		}
		System.out.println("Size: " + sequence.size());
		return currentFloor;
	}

	/**
	 * adds floor to visit to sequence arraylist
	 * 
	 * @param floor
	 * @return
	 */
	public boolean setFloor(int floor) {
		inButtons[floor - 1] = true; // was used in old move() to flag floors true if they were to be visited
		sequence.add(floor);
		return true;
	}

	public int getFloor() {
		return currentFloor;
	}

	/**
	 * return direction
	 */
	public int getDirection() {
		if (direction == 1) {
			return UP;
		}
		if (direction == 2) {
			return DOWN;
		}
		return NOT_SET;
	}

}
