package theAtm;
/**
 * Transaction objects are created and stored in an array list.
 * Transaction information is written to transactions.txt.
 * 
 *  @author Greg Kline
 */
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class TransactionFile {

	private ArrayList<TransactionData> transactions;
	AccountFile theAccounts = new AccountFile();
	ArrayList<AccountData> accounts = theAccounts.readFile();
	int day;
	public TransactionFile() {
		transactions = new ArrayList<TransactionData>();
		theAccounts.readFile();
	}
	/*
	 * Opens transaction file and makes each transaction its own object to store in transactions array.
	 * Returns the transaction array.
	 */
	public ArrayList<TransactionData> readTrans() {

		Scanner scan = null;
		try {
			FileInputStream file = new FileInputStream("Transactions.txt");
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		while (scan.hasNext()) {
			TransactionData transData = new TransactionData();
			transData.setID(scan.nextLine());
			transData.setDate(scan.nextLine());
			transData.setAmount(scan.nextLine());
			transData.setDeposit(scan.nextLine());
			transData.setWithdraw(scan.nextLine());
			transData.setDay(scan.nextLine());
			
			transactions.add(transData);
		}
		scan.close();
		return transactions;
	}
	/*
	 * Write to the transaction file, does not overwrite any data
	 */
	public void writeTrans() {
		FileWriter output = null;
		try {
			output = new FileWriter("Transactions.txt");
			BufferedWriter file = new BufferedWriter(output);
			for (int i = 0; i < transactions.size(); i++) {
				file.write(transactions.get(i).getID());
				file.write("\n" + transactions.get(i).getDate());
				file.write("\n" + transactions.get(i).getAmount());
				file.write("\n" + transactions.get(i).getDeposit());
				file.write("\n" + transactions.get(i).getWithdraw());
				file.write("\n" + transactions.get(i).getDay() + "\n");
			}
			file.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Takes in an amount to deposit and an account index
	 * returns the balance after the transaction takes place.
	 * Also updates the date.
	 * @param amount
	 * @param accIndex
	 * @return
	 */
	public String deposit(float amount, int accIndex) {
		Calendar calendar = new GregorianCalendar(2017, 5, day);
		String accID = accounts.get(accIndex).getID();
		TransactionData transaction = new TransactionData();
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
		day+=1;
		String theDay = Integer.toString(day);
		/*
		 * takeOut is the string version of the deposit amount that is passed in
		 * balance is the amount after the transaction afterBalance is the
		 * string version of balance
		 */
		String takeIn = Float.toString(amount);
		float balance = Float.parseFloat(accounts.get(accIndex).getBalance()) + amount;
		String afterBalance = Float.toString(balance);
		transaction.setID(accID);
		transaction.setDeposit(takeIn);
		transaction.setDate(format.format(calendar.getTime()));
		transaction.setAmount(afterBalance);
		transaction.setDay(theDay);
		/*
		 * should write over account balance
		 */
		accounts.get(accIndex).setBalance(afterBalance);
		transactions.add(transaction);
		theAccounts.writeFile(accounts);
		return afterBalance;
	}
	/**
	 * Takes in an amount to withdraw and an account index
	 * returns the balance after the transaction takes place
	 * Also updates the date.
	 * @param amount
	 * @param accIndex
	 * @return
	 */
	public String withdraw(float amount, int accIndex) {
		Calendar calendar = new GregorianCalendar(2017, 5, day);
		String accID = accounts.get(accIndex).getID();
		TransactionData transaction = new TransactionData();
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
		day+=1;
		String theDay = Integer.toString(day);
		/*
		 * takeOut is the string version of the withdraw amount that is passed
		 * in balance is the amount after the transaction afterBalance is the
		 * string version of balance
		 */
		String takeOut = Float.toString(amount);
		float balance = Float.parseFloat(accounts.get(accIndex).getBalance()) - amount;
		String afterBalance = Float.toString(balance);
		transaction.setID(accID);
		transaction.setWithdraw(takeOut);
		transaction.setDate(format.format(calendar.getTime()));
		transaction.setAmount(afterBalance);
		transaction.setDay(theDay);
		/*
		 * should write over account balance
		 */
		accounts.get(accIndex).setBalance(afterBalance);
		transactions.add(transaction);
		theAccounts.writeFile(accounts);
		return afterBalance;
	}

}
