package theAtm;
/**
 * GUI for the ATM.
 * Includes numerical buttons to enter pin, and clear button to easily clear the text area for user input.
 * 
 * @author Seth Callan
 * @author Greg Kline
 */
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class View extends JFrame {
	Controller control = new Controller();

	/*
	 * Properties
	 */

	JFrame window = new JFrame("Virtual ATM");
	JPanel myLeftPanel = new JPanel();
	JPanel myRightPanel = new JPanel();
	JPanel myBottomPanel = new JPanel();
	JTextArea textArea1 = new JTextArea();
	int theDay;
	int accIndex = -1;
	TransactionFile transactions = new TransactionFile();

	public View() {

		// Panel
		window.add(myLeftPanel, BorderLayout.CENTER);
		myLeftPanel.setLayout(new GridLayout(3, 2));
		// myLeftPanel.setSize(new Dimension(225, 700));
		myLeftPanel.setBackground(Color.GRAY);

		window.add(myRightPanel, BorderLayout.EAST);
		myRightPanel.setLayout(new GridLayout(5, 3, 10, 10));

		// Key pad buttons
		JButton one = new JButton("1");
		JButton two = new JButton("2");
		JButton three = new JButton("3");
		JButton four = new JButton("4");
		JButton five = new JButton("5");
		JButton six = new JButton("6");
		JButton seven = new JButton("7");
		JButton eight = new JButton("8");
		JButton nine = new JButton("9");
		JButton zero = new JButton("0");
		JButton clear = new JButton("Clear");
		// JButton enter = new JButton("Enter");

		// add keypad buttons to panel
		myRightPanel.setBackground(Color.WHITE);
		myRightPanel.add(one);
		myRightPanel.add(two);
		myRightPanel.add(three);
		myRightPanel.add(four);
		myRightPanel.add(five);
		myRightPanel.add(six);
		myRightPanel.add(seven);
		myRightPanel.add(eight);
		myRightPanel.add(nine);
		myRightPanel.add(zero);
		myRightPanel.add(clear);
		// myRightPanel.add(enter);

		window.add(myBottomPanel, BorderLayout.WEST);

		// user buttons
		JButton login, logout, deposit, withdraw, balance, tHistory, dHistory, wHistory, id, pin;
		login = new JButton("LOGIN");
		logout = new JButton("LOG OUT");
		deposit = new JButton("DEPOSIT");
		withdraw = new JButton("WITHDRAW");
		balance = new JButton("BALANCE");
		tHistory = new JButton("TRANSACTION HISTORY");
		dHistory = new JButton("DEPOSIT HISTORY");
		wHistory = new JButton("WITHDRAW HISTORY");
		id = new JButton("ID");
		pin = new JButton("PIN");

		login.setEnabled(true);
		logout.setEnabled(false);
		deposit.setEnabled(false);
		withdraw.setEnabled(false);
		balance.setEnabled(false);
		tHistory.setEnabled(false);
		dHistory.setEnabled(false);
		wHistory.setEnabled(false);
		id.setEnabled(false);
		pin.setEnabled(false);

		JPasswordField accountid = new JPasswordField(10);
		accountid.setActionCommand("OK");
		accountid.setEnabled(true);

		JPasswordField accountpin = new JPasswordField(10);
		accountpin.setActionCommand("OK");
		accountpin.setEnabled(true);

		// add user buttons to panel
		myBottomPanel.setLayout(new GridLayout(6, 2, 15, 15));
		myBottomPanel.setSize(500, 400);
		myBottomPanel.setBackground(Color.LIGHT_GRAY);
		myBottomPanel.add(login);
		myBottomPanel.add(logout);
		myBottomPanel.add(deposit);
		myBottomPanel.add(withdraw);
		myBottomPanel.add(balance);
		myBottomPanel.add(tHistory);
		myBottomPanel.add(dHistory);
		myBottomPanel.add(wHistory);
		myBottomPanel.add(id);
		myBottomPanel.add(pin);
		myBottomPanel.add(accountid);
		myBottomPanel.add(accountpin);

		window.add(textArea1, BorderLayout.SOUTH);
		textArea1.setSize(25, 25);
		textArea1.setFont(textArea1.getFont().deriveFont(20f));
		textArea1.setLineWrap(true);
		textArea1.setEditable(true);
		textArea1.setVisible(true);
		JScrollPane scroll = new JScrollPane(textArea1);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		window.add(scroll);
		
		// numerical buttons

		one.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "1");
			}
		});

		two.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "2");
			}
		});

		three.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "3");
			}
		});

		four.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "4");
			}
		});

		five.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "5");
			}
		});

		six.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "6");
			}
		});

		seven.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "7");
			}
		});

		eight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "8");
			}
		});

		nine.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "9");
			}
		});

		zero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String currentText = accountpin.getText();
				accountpin.setText(currentText + "0");
			}
		});

		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea1.setText("");
			}
		});

		window.setSize(900, 650);
		window.setDefaultCloseOperation(EXIT_ON_CLOSE);
		window.setVisible(true);

		// Operation Buttons
		
		login.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
				String loginID = new String(accountid.getPassword());
				String loginPin = new String(accountpin.getPassword());
				accountpin.getPassword();
				if (Integer.parseInt(loginID) == 5 && Integer.parseInt(loginPin) == 1234) {
					accIndex = 0;
					login.setEnabled(false);
					logout.setEnabled(true);
					accountpin.setEnabled(false);
					accountid.setEnabled(false);
				} else if (Integer.parseInt(loginID) == 6 && Integer.parseInt(loginPin) == 4324) {
					accIndex = 1;
					login.setEnabled(false);
					logout.setEnabled(true);
					accountpin.setEnabled(false);
					accountid.setEnabled(false);
				} else if (Integer.parseInt(loginID) == 7 && Integer.parseInt(loginPin) == 5687) {
					accIndex = 2;
					login.setEnabled(false);
					logout.setEnabled(true);
					accountpin.setEnabled(false);
					accountid.setEnabled(false);
				} else {
					textArea1.setText("Invalid Login");
				}
				if (control.authentication(accIndex) > -1) {
					id.setEnabled(true);
					pin.setEnabled(true);
					one.setEnabled(false);
					two.setEnabled(false);
					three.setEnabled(false);
					four.setEnabled(false);
					five.setEnabled(false);
					six.setEnabled(false);
					seven.setEnabled(false);
					eight.setEnabled(false);
					nine.setEnabled(false);
					zero.setEnabled(false);
					deposit.setEnabled(true);
					withdraw.setEnabled(true);
					balance.setEnabled(true);
					tHistory.setEnabled(true);
					dHistory.setEnabled(true);
					wHistory.setEnabled(true);
					control.makeDeposit(0, accIndex);
					control.makeWithdraw(0, accIndex);
					control.readAccounts();
					control.readTransactions();

					textArea1.setText("Welcome " + control.person(accIndex) + "\n" + "Press clear and enter a number"
							+ "\n" + "to deposit or withdraw." + "\n"
							+ "Once entered, select your desiredtransaction.");
					
				}
				}catch( java.lang.NumberFormatException r){
					textArea1.setText("Invalid Login.");
				}
				// textArea1.setText("Please Enter Account ID in the field
				// below.");
				// accountid.setEnabled(false);

			}
		});

		logout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login.setEnabled(true);
				logout.setEnabled(false);
				deposit.setEnabled(false);
				withdraw.setEnabled(false);
				balance.setEnabled(false);
				tHistory.setEnabled(false);
				dHistory.setEnabled(false);
				wHistory.setEnabled(false);
				id.setEnabled(false);
				pin.setEnabled(false);
				textArea1.setText("Log out successful.");
				accountpin.setEnabled(true);
				accountid.setEnabled(true);
				one.setEnabled(true);
				two.setEnabled(true);
				three.setEnabled(true);
				four.setEnabled(true);
				five.setEnabled(true);
				six.setEnabled(true);
				seven.setEnabled(true);
				eight.setEnabled(true);
				nine.setEnabled(true);
				zero.setEnabled(true);
				accountid.setText("");
				accountpin.setText("");
			}
		});
		deposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float amount = Float.parseFloat(textArea1.getText());
				transactions.readTrans().get(transactions.readTrans().size() - 1).getDay();
				control.makeDeposit(amount, accIndex);
				control.writeTransactions();
				textArea1.setText("Deposit of " + amount + " made.");

			}
		});

		withdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float amount = Float.parseFloat(textArea1.getText());
				control.makeWithdraw(amount, accIndex);
				control.writeTransactions();
				textArea1.setText("Withdraw of " + amount + " made.");

			}
		});

		balance.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int len = transactions.readTrans().size();
				int j = len - 1;
				int latestTransIndex = transactions.readTrans().size() - 1;
				textArea1.setText("BALANCE");
				String currentText = textArea1.getText();
				textArea1.setText(currentText + "\n" + (transactions.readTrans().get(latestTransIndex).getAmount()));

				while (j < len) {
					if (Integer.parseInt(transactions.readTrans().get(j).getID()) == Integer
							.parseInt(transactions.accounts.get(accIndex).getID())) {
						if (Objects.equals(String.valueOf(transactions.readTrans().get(j).getAmount()),
								"null") != true) {
							textArea1.setText(currentText + "\n" + transactions.readTrans().get(j).getAmount());
							break;
						}
					}
					j--;
				}

			}
		});

		tHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//The transactions are read from last to first, so J is the index of the latest transaction
				int counter = 0;
				int len = transactions.readTrans().size();
				int j = len - 1;
				textArea1.setText("TRANSACTION HISTORY");
				while (j > 0) {
					// checks if the value of the user ID matches the ID of the transaction object
					if (Integer.parseInt(transactions.readTrans().get(j).getID()) == Integer
							.parseInt(transactions.accounts.get(accIndex).getID()) && counter < 10) {
						String currentText = textArea1.getText();
						// If the value at the deposit isn't null, it's a deposit.
						if (Objects.equals(String.valueOf(transactions.readTrans().get(j).getDeposit()),
								"null") != true) {
							textArea1.setText(currentText + "\n" + "D: " + transactions.readTrans().get(j).getDeposit()
									+ " " + transactions.readTrans().get(j).getDate() + "\n");
							counter++;
							// If the value at the withdraw isn't null, it's a deposit.
						} else if (Objects.equals(String.valueOf(transactions.readTrans().get(j).getWithdraw()),
								"null") != true) {
							textArea1.setText(currentText + "\n" + "W: " + transactions.readTrans().get(j).getWithdraw()
									+ " " + transactions.readTrans().get(j).getDate() + "\n");
							counter++;
						}
					}
					j--;
					if (counter >= 10) {
						break;
					}

				}

			}
		});

		dHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*
				 * The transactions are read from last to first, so J is the index of the latest transaction
				 */
				int counter = 0;
				int j = 0;
				int len = transactions.readTrans().size();
				j = len - 1;
				textArea1.setText("DEPOSIT HISTORY");
				while (j > 0) {
					// checks if the value of the user ID matches the ID of the transaction object
					if (Integer.parseInt(transactions.readTrans().get(j).getID()) == Integer
							.parseInt(transactions.accounts.get(accIndex).getID()) && counter < 10) {
						String currentText = textArea1.getText();
						/* If the value at the deposit isn't null, it's a deposit.
						 * If the deposit is null than that transaction was a withdraw instead, so it doesn't print it and only increments j
						 */
						if (Objects.equals(String.valueOf(transactions.readTrans().get(j).getDeposit()),
								"null") != true) {
							textArea1.setText(currentText + "\n" + transactions.readTrans().get(j).getDeposit() + " "
									+ transactions.readTrans().get(j).getDate() + "\n");
							counter++;
						}
					}
					j--;
					if (counter >= 10) {
						break;
					}

				}

			}
		});

		wHistory.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int counter = 0;
				int j = 0;
				int len = transactions.readTrans().size();
				j = len - 1;
				textArea1.setText("");
				textArea1.setText("WITHDRAW HISTORY");
				while (j > 0) {
					if (Integer.parseInt(transactions.readTrans().get(j).getID()) == Integer
							.parseInt(transactions.accounts.get(accIndex).getID()) && counter < 10) {
						String currentText = textArea1.getText();

						if (Objects.equals(String.valueOf(transactions.readTrans().get(j).getWithdraw()),
								"null") != true) {
							textArea1.setText(
									currentText + "\n" + "Withdraw: " + transactions.readTrans().get(j).getWithdraw()
											+ " " + "Date: " + transactions.readTrans().get(j).getDate() + "\n");
							counter++;
						}
					}
					j--;
					if (counter >= 10) {
						break;
					}

				}

			}
		});
	}
}
