package theAtm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Account objects are created and stored in an array list.
 * Account information is written to accounts.txt
 * 
 * @author Greg Kline
 *
 */

public class AccountFile {

	private ArrayList<AccountData> accounts;

	public AccountFile() {
		accounts = new ArrayList<AccountData>();
	}

	//Method to open account file and set  information to each account, and add each account to the array list.
	public ArrayList<AccountData> readFile() {

		Scanner scan = null;
		try {
			FileInputStream file = new FileInputStream("Accounts.txt");
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (scan.hasNext()) {
			AccountData account = new AccountData();
			account.setID(scan.nextLine());
			account.setPin(scan.nextLine());
			account.setFirstName(scan.nextLine());
			account.setLastName(scan.nextLine());
			account.setBalance(scan.nextLine());
			accounts.add(account);

		}
		scan.close();
		return accounts;
	}
	// Method to get the set account information, and print to accounts.txt. This overwrites the file.
	public void writeFile(ArrayList<AccountData> data) {
		FileWriter output = null;
		PrintWriter file = null;
		try {
			file = new PrintWriter(new FileOutputStream("Accounts.txt", false));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < accounts.size() / 2; i++) {
			file.print(accounts.get(i).getID());
			file.print("\n" + accounts.get(i).getPin());
			file.print("\n" + accounts.get(i).getFirst());
			file.print("\n" + accounts.get(i).getLast());
			file.print("\n" + accounts.get(i).getBalance() + "\n");
		}
		file.close();
	}
}
