/**
 * ATM that keeps track of 3 accounts. including their balances, transactions, and withdraws.
 * The user must enter an account ID and PIN to log in.
 * Each accounts contains a first name, last name, ID, pin, and balance that are stored in a AccountData object.
 * The accountsData objects are then stored in an array list.
 * 
 * The three IDs and Pins are:
 * 5	1234
 * 6	4324
 * 7	5687
 * 
 * Project Due Date: April 29th, 2017
 * 
 * @author Greg Kline
 * @author Seth Callan
 */
package theAtm;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		
		View vw = new View();
	}

}
