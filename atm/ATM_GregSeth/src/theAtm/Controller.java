package theAtm;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * Methods to tell the program to read the account or transaction file, 
 * write to the transaction file, make a deposit or withdraw, check if a user is logged in, 
 * and return an account holder's full name.
 * @author Greg Kline
 *
 */
public class Controller {
	AccountFile accounts = new AccountFile();
	TransactionFile transactions = new TransactionFile();
	ArrayList<TransactionData> test = new ArrayList<TransactionData>();

	public Controller() {

	}

	public void readAccounts() {
		accounts.readFile();
	}

	public void readTransactions() {
		transactions.readTrans();
	}

	public void writeTransactions() {
		transactions.writeTrans();
	}
	/**
	 * Take in a deposit amount and an account's index in the array list.
	 * @param amount
	 * @param accIndex
	 * @return
	 */
	public String makeDeposit(float amount, int accIndex) {
		return transactions.deposit(amount, accIndex);
	}
	/**
	 * Take in a amount to withdraw and an account's index in the array
	 * @param amount
	 * @param accIndex
	 */
	public void makeWithdraw(float amount, int accIndex) {
		transactions.withdraw(amount, accIndex);
	}
	/**
	 * Takes in an account index, returns that account's full name
	 * @param accIndex
	 * @return
	 */
	public String person(int accIndex) {
		transactions.accounts.get(accIndex).getFirst();
		String fullName = transactions.accounts.get(accIndex).getFirst() + " "
				+ transactions.accounts.get(accIndex).getLast();
		return fullName;
	}
	/**
	 * Takes in an account index, checks if the account at that index matches any of the 
	 * IDs and Pins, and if it does return a value greater than 0.
	 * @param accIndex
	 * @return
	 */
	public int authentication(int accIndex) {
		int theCode = -1;
		readAccounts();
		int theID = Integer.parseInt(transactions.accounts.get(accIndex).getID());
		int thePin = Integer.parseInt(transactions.accounts.get(accIndex).getPin());
		System.out.println(thePin);
		if (thePin == 1234 && theID == 5) {
			theCode = 1;
		}

		else if (thePin == 4324) {
			theCode = 2;
		}

		else if (thePin == 5687) {
			theCode = 3;
		}
		return theCode;
	}

}
