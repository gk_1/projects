/**
 * Setters and getters for withdraw, deposit, amount, date, id, and the day a transaction took place.
 * 
 * @author Greg Kline
 */
package theAtm;

public class TransactionData {
	private String withdraw;
	private String deposit;
	private String amount;
	private String date;
	private String id;
	private String day;

	public TransactionData() {

	}

	public void setDay(String myDay)
	{
		day = myDay;
	}
	
	public void setID(String myID)
	{
		id = myID;
	}
	
	public void setWithdraw(String myWithdraw) {
		withdraw = myWithdraw;
	}

	public void setDeposit(String myDeposit) {
		deposit = myDeposit;
	}

	public void setAmount(String myAmount) {
		amount = myAmount;
	}

	public void setDate(String myDate) {
		date = myDate;
	}

	public String getWithdraw() {
		return withdraw;
	}

	public String getDeposit() {
		return deposit;
	}

	public String getAmount() {
		return amount;
	}

	public String getDate() {
		return date;
	}
	
	public String getID(){
		return id;
	}
	
	public String getDay()
	{
		return day;
	}

}
