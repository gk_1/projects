/**
 * Setters and getters for ID, PIN, First name, and last name
 * 
 * @author Greg Kline
 */
package theAtm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class AccountData {
	private String id;
	private String pin;
	private String first;
	private String last;
	private String balance;

	public AccountData() {

	}

	public void setID(String myId) {
		id = myId;

	}

	public void setPin(String myPin) {
		pin = myPin;

	}

	public void setFirstName(String myFirst) {
		first = myFirst;
	}

	public void setLastName(String myLast) {
		last = myLast;
	}

	public void setBalance(String myBalance) {
		balance = myBalance;

	}

	public String getID() {
		return id;

	}

	public String getPin() {
		return pin;

	}

	public String getFirst() {
		return first;
	}

	public String getLast() {
		return last;
	}

	public String getBalance() {
		return balance;
	}
}
