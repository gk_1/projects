#include "player.h"
#include "character.h"
#include "location.h"
#include "item.h"
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
Location locations[11];
Player plays[2];
Player monster;
Item items[3];
Location town;
string message;
int monsterL;
int dest = 0;
bool inGame = true;

int wander(int a, int b) {
	monsterL = rand() % a + b;
	return monsterL;
}

void parse(string m, int j) {

	if (m.find("Where") != std::string::npos
			|| m.find("where") != std::string::npos) {
		cout << " " << locations[plays[j].getLocation() - 1].getDesc() << " "
				<< endl;

	}

	if (m.find("inventory") != std::string::npos
			|| m.find("Inventory") != std::string::npos) {
		plays[j].showInventory();

	}

	else if (m.find("take") != std::string::npos
			|| m.find("Take") != std::string::npos) {
		if (plays[j].getLocation() == 2) {
			locations[plays[j].getLocation() - 1].removeItem("Apple", plays[j]);
		} else if (plays[j].getLocation() == 3) {
			locations[plays[j].getLocation() - 1].removeItem("Knife", plays[j]);
		}
		cout << "Items: ";
		for (int i = 0; i < 3; i++) {
			cout << plays[j].inventory[i].getName() << " ";
		}
		cout << endl;
	}

	if (m.find("look") != std::string::npos
			|| m.find("Look") != std::string::npos) {
		cout << locations[plays[j].getLocation() - 1].getContents() << endl;

	}

	if (m.find("attack") != std::string::npos) {
		if (plays[j].getLocation() == monster.getLocation()) {
			if (plays[j].checkItem(items[2].getName())) {
				plays[j].attack(monster, 20);
				cout << "Minotaur remaining HP: " << monster.getHP() << endl;
				if (monster.getHP() < 0) {
					cout << "The Minotaur has already been slain!" << endl;
				}
			} else {
				cout << "You need a weapon to fight!";
			}
		}

	}

	else if (m.find("help") != std::string::npos
			|| m.find("Help") != std::string::npos
			|| m.find("?") != std::string::npos) {
		cout
				<< "Go: Move forward \nAttack: Attack a monster \nWhere: Description of your surroundings \nTake: take an item \nLook: Check for take-able items "
				<< endl;

	} else if (m.find("Go") != std::string::npos
			|| m.find("go") != std::string::npos) {
		cin >> m;
		locations[5].addChar(monster);
		if (m.find("Start") != std::string::npos) {
			//cout << "MONSTER LOCATION: " << wander(0,9) << endl;
			locations[0].setAvailable(0);
			locations[1].setAvailable(0);
			locations[4].setAvailable(0);
			locations[0].addChar(plays[j]);
		} else if (m.find("Mossy") != std::string::npos) {
			if (locations[0].getAvailable() || locations[3].getAvailable()) {

				locations[0].setAvailable(0);
				locations[3].setAvailable(0);
				locations[1].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}

		} else if (m.find("Dark") != std::string::npos) {
			if (locations[8].getAvailable() || locations[6].getAvailable()) {
				locations[6].setAvailable(0);
				locations[8].setAvailable(0);
				locations[2].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}
		} else if (m.find("Generic") != std::string::npos) {
			if (locations[1].getAvailable() || locations[6].getAvailable()) {
				locations[1].setAvailable(0);
				locations[6].setAvailable(0);
				locations[3].addChar(plays[j]);
			}
			if (plays[j].getLocation() == monster.getLocation()) {
				cout << "You heard the roar of a minotaur!" << endl;
			}

		} else if (m.find("Rainbow") != std::string::npos) {
			if (locations[8].getAvailable() || locations[6].getAvailable()
					|| locations[7].getAvailable()) {
				locations[0].setAvailable(0);
				locations[5].setAvailable(0);
				locations[7].setAvailable(0);
				locations[4].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}
		} else if (m.find("Alley") != std::string::npos) {
			if (locations[4].getAvailable() || locations[6].getAvailable()) {
				locations[4].setAvailable(0);
				locations[6].setAvailable(0);
				locations[5].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}
		} else if (m.find("Haunted") != std::string::npos) {
			if (locations[3].getAvailable() || locations[2].getAvailable()
					|| locations[5].getAvailable()) {
				locations[5].setAvailable(0);
				locations[3].setAvailable(0);
				locations[2].setAvailable(0);
				locations[6].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}

		} else if (m.find("Sewer") != std::string::npos) {
			if (locations[8].getAvailable() || locations[4].getAvailable()) {
				locations[4].setAvailable(0);
				locations[8].setAvailable(0);
				locations[7].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}
		} else if (m.find("Well-lit") != std::string::npos) {
			if (locations[2].getAvailable() || locations[9].getAvailable()) {
				locations[2].setAvailable(0);
				locations[9].setAvailable(0);
				locations[8].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
			}
		} else if (m.find("Minotaur") != std::string::npos) {
			if (locations[8].getAvailable()) {
				locations[8].setAvailable(0);
				locations[9].addChar(plays[j]);
				if (plays[j].getLocation() == monster.getLocation()) {
					cout << "You heard the roar of a minotaur!" << endl;
				}
				if (monster.getHP() < 0) {
					cout
							<< "Congratulations! You've made it out of the Labyrinth!"
							<< endl;
					inGame = false;
				}
			}
			locations[dest].addChar(plays[j]);
		}

	}
}
int main() {
	int size;
	string name;
	cout << "Enter # of players: " << endl;
	cin >> size;
	for (int i = 0; i < size; i++) {
		cout << "Enter name for player #" << i + 1 << endl;
		cin >> name;
		plays[i].setName(name);
	}

	monster.setHP(200);
	monster.setName("the Minotaur");

	locations[0].setInfo(1, " Start of the Labyrinth");
	locations[0].setDesc(
			" You're at the center of the great Labyrinth! Beware of wandering minotaurs and shifting corridors! You can see a Mossy Corridor up ahead! ");

	locations[1].setInfo(2, " Mossy Corridor ");
	locations[1].setDesc(
			" You can see green moss growing on the walls of the labyrinth. There is an apple on the floor. You can head back to the the start or go towards the Generic Corridor ");
	locations[2].setInfo(3, " Darkened Corridor ");
	locations[2].setDesc(
			" You can see absolutely nothing! You can hear shreaks coming from what seems to be a Haunted Corridor and can barely make out a distant Alley  ");
	locations[3].setInfo(4, " Generic Corridor ");
	locations[3].setDesc(
			" You feel like you've been here before. You can head towards the mossy corridor or enter the Haunted Corridor ");

	locations[4].setInfo(5, " Rainbow Corridor ");
	locations[4].setDesc(
			" You're walking on sunshine, and it burns. You can go to an Alley, head to the Start, or enter the Sewer. ");
	locations[5].setInfo(6, " Alley ");
	locations[5].setDesc(
			" Not a place you would want to be at night. From here you can head into the Haunted Corridor or Rainbow Corridor ");
	locations[6].setInfo(7, " Haunted Corridor ");
	locations[6].setDesc(
			" You hear the shrieks of the undead. You don't want to stick around long. You can go towards the Dark Corridor, Generic Corridor, or Alley ");

	locations[7].setInfo(8, " Sewer tunnel ");
	locations[7].setDesc(
			" You wonder why a labyrinth would even need a sewer. You can see a rainbow at one end of the sewer and a Well-lit cave at the other  ");
	locations[8].setInfo(9, " Well-lit Cave ");
	locations[8].setDesc(
			" Surprisingly well lit for a cave. Torches are placed haphazardly on the walls. You can go towards the Minotaur's domain or head back to the Well-lit cave ");
	locations[9].setInfo(10, " Minotaur's domain ");
	locations[9].setDesc(
			" There's nothing but blood, bones, and unrelenting fear. You can fight the Minotaur to win OR go towards the well-lit cave. ");
	locations[9].addChar(monster);
	locations[9].setInfo(10, " Minotaur's domain ");
	locations[9].setDesc(
			" There's nothing but blood, bones, and unrelenting fear. If you've killed the Minotaur, the exit should be open. If not, you can head towards the well-lit cave. T");
	locations[10].setInfo(11, " Minotaur's domain ");
	locations[10].setDesc(
			" There's nothing but blood, bones, and unrelenting fear. You can fight the Minotaur to win OR go towards the well-lit cave. ");

	locations[5].addChar(monster);

	items[0].setName("Apple");
	items[1].setName("Pair");
	items[9].setName("Knife");

	//put items in locations

	locations[1].addItem(items[0]);
	locations[2].addItem(items[9]);
	cout
			<< "Now entering the game. Type help for list of commands. Begin by typing 'Go Start'";
	int playTurn = 0;
	while (inGame == true) {
		cin >> message;
		parse(message, playTurn);
		if (message == "Stop") {
			inGame = false;
		}

	}

	return 0;
}

