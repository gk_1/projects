#include "character.h"

Character::Character(){
	 hp = 100;
	 location =0;
	name = "player";
	itemCount = 0;
	dead = false;
}

int Character::getHP(){
	return hp;
}


void Character::setHP(int newHP){
	hp = newHP;
}

void Character::kill(){
	dead = true;
}

bool Character::isAlive(){
	return dead;
}

std::string Character::getName(){
	return name;
}

bool Character::checkItem(std::string item){
	for(int i =0;i<itemCount;i++){
		if(inventory[i].getName() == item){
			return true;
		}
	}
	return false;
}

void Character::setName(std::string newName){
	name = newName;
}

// should only use these inside location

void Character::setLocation(int thelocation){
	location = thelocation;
}

int Character::getLocation(){
	return location;
}
void Character::addItem(Item& theItem){
	inventory[itemCount] = theItem;
	itemCount++;

}

void Character::showInventory(){
	for(int i  =0; i < itemCount; i++){
		cout << inventory[i].getName();
	}
}

Item Character::getInventory(){
	return *inventory;
}

Character::~Character(){

}

//void Character::setLocation(Location &dest){
//	room = dest;
//}
//
//void Character::getLocation(){
//}
