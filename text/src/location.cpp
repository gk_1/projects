#include "location.h"

Location::Location() {
	townName = "noName";
	townCode = 1;
	available = false;
}

void Location::addChar(Character &oneChar) {
	if (available == true) {
		//chars.push_back(&oneChar);
		//chars.back()->setLocation(townCode);
		oneChar.setLocation(townCode);
		cout << "You are now in the " << getTownName() << endl;
		//cout << chars.back()->getName() << endl;
		//cout << chars.back()->getName() << " entering" << getTownName() << " IN LOC: " << chars.back()->getLocation();
	}
}

void Location::setAvailable(int ava) {
	if (ava == 0) {
		available = true;
	} else {
		available = false;
	}
}

bool Location::getAvailable() {
	return available;
}

void Location::setInfo(int myCode, std::string myName) {
	townName = myName;
	townCode = myCode;
}

std::string Location::getTownName() {
	return townName;
}

void Location::setTownName(std::string name) {
	townName = name;
}

void Location::setTownCode(int code) {
	townCode = code;
}

std::string Location::entering() {
	return chars.back()->getName() + " has entered the " + townName;
}

void Location::setDesc(std::string mes) {
	desc = mes;
}

std::string Location::getDesc() {
	std::string descript;
	return desc;
}

void Location::addItem(Item& item) {
	inventory[itemCount] = item;
	itemCount++;
}

Item Location::getInventory() {
	return *inventory;
}

std::string Location::getContents() {
	std::string descript;
	for (int i = 0; i <= itemCount; i++) {
		descript += inventory[i].getName() + " ";
	}
	return descript;
}

void Location::removeItem(std::string itemsName, Character &charac) {
	for (int i = 0; i < itemCount; i++) {
		if (inventory[i].getName() == itemsName) {
			//chars.back()->addItem(inventory[i]);
			Item newItem;
			newItem.setName(inventory[i].getName());
			charac.addItem(newItem);
			inventory[i].setName("");
			break;
		}
	}
}
Location::~Location(){
}

