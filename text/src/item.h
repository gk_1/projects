#ifndef ITEM_H
#define ITEM_H
#include <string>
#include <iostream>
//#include <location.h>
using namespace std;
class Item
{
    private:
	string name;
	string desc;
    public:
	void setName(std::string);
	std::string  getName();
	~Item();

};

#endif
