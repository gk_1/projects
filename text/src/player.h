#ifndef PLAYER_H
#define PLAYER_H
#include <string>
#include "character.h"
#include "item.h"

class Player : public Character
{
    private:
    public:
	Player();
	void addItem(Item&);
	void attack(Character&, int);
	~Player();
};

#endif
