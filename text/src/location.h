#ifndef LOCATION_H
#define LOCATION_H
#include <vector>
#include <iostream>
#include "character.h"
#include "item.h"
using namespace std;
//class Character;

class Location {
private:
	//std::vector<Character> curChars;
	Character *characs = new Character[5];
	std::vector<Character*> chars;
	std::string townName;
	std::string desc;
	int itemCount =0;
	bool available;

	int townCode;
public:
	Item *inventory = new Item[10];
	Location();
	void setInfo(int, std::string);
	void setTownCode(int);
	void setTownName(std::string);
	std::string getTownName();
	void addChar(Character&);
	void copyIt();
	std::string entering();
	std::string getDesc();
	void setDesc(std::string);
	void addItem(Item&);
	Item getInventory();
	std::string getContents();
	void removeItem(std::string, Character&);
	void setAvailable(int);
	bool getAvailable();
	~Location();
};

#endif
