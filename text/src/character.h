#ifndef CHARACTER_H
#define CHARACTER_H
#include <string>
#include <iostream>
#include "item.h"
using namespace std;
class Character {
private:
	int hp;
	int location;
	std::string name;
	int itemCount;
	bool dead;

//	Location room;
public:
	Character();
	Item *inventory = new Item[10];
	void setHP(int);
	void act();
	int getHP();
	int getLocation();
	std::string getName();
	bool checkItem(std::string);
	void setName(std::string);
	void attack(Character&, int);
	void setLocation(int);
	void showInventory();
	Item getInventory();
	void addItem(Item&);
	void kill();
	bool isAlive();
	~Character();
//	void setLocation(Location&);
//	void getLocation();
};

#endif
