package nqueens;

import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.util.Scanner;
import java.util.Vector;
import java.awt.Point;

//import javax.swing.ImageIcon;
//import javax.swing.JFrame;
//import javax.swing.JLabel;
//import javax.swing.JOptionPane;
//import javax.swing.JPanel;
import javax.swing.*;

/**
 * View class takes user input via JOptionPane to get board size.
 * Red and black images are placed based on coordinates, which are based on the board size.
 * Queens are placed based on coordinates determined in the model
 * 
 * @author Greg Kline
 *
 */

public class View {
	int x_missing;
	int y_missing;
	int board_size;
	int counter = 0;
	public JOptionPane pane;
	JLabel missingL;
	ImageIcon B, W, Q;
	Boolean color = true;

	public View(Controller controller) {
		B = new ImageIcon("images/B.PNG");
		W = new ImageIcon("images/W.PNG");
		Q = new ImageIcon("images/Q.PNG");
		JFrame frame = new JFrame("nQueens Demo");
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		String bs = JOptionPane.showInputDialog("Enter the board size ");
		board_size = Integer.parseInt(bs);
		while(board_size > 8 || board_size < 3 || board_size % 2 != 0) {
			bs = JOptionPane.showInputDialog("Enter the board size ");
			board_size = Integer.parseInt(bs);
		}
		controller.tile(board_size);
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(board_size * 100, board_size * 100));
		panel.setBackground(Color.WHITE);
		panel.setLayout(null);
		Dimension panelD = new Dimension(90, 90);
		controller.startQueen();
		
			// place images
		for (int i = 0; i < controller.getCoords().size(); i++) {
			
			if (i < controller.getQueens().size()) {
				JLabel queen = new JLabel(Q);
				panel.add(queen);
				queen.setSize(panelD);
				queen.setLocation((controller.getQueens().get(i).x * 92), ((controller.getQueens().get(i).y * 92)));
			}
			if (color == true) {
				JLabel lab = new JLabel(B);
				panel.add(lab);
				lab.setSize(panelD);
				lab.setLocation((controller.getCoords().get(i).x * 92), ((controller.getCoords().get(i).y * 92)));
				if (controller.getCoords().get(i).y < Math.sqrt(controller.getCoords().size()) - 1) {
					color = false;
				}

			} else if (color == false) {
				JLabel lab = new JLabel(W);
				panel.add(lab);
				lab.setSize(panelD);
				lab.setLocation((controller.getCoords().get(i).x * 92), ((controller.getCoords().get(i).y * 92)));
				if (controller.getCoords().get(i).y < Math.sqrt(controller.getCoords().size()) - 1) {
					color = true;
				}
			}
		}
		frame.getContentPane().add(panel);
		frame.pack();

	}
}
