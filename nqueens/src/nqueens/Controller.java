package nqueens;

import java.awt.Point;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Vector;
/**
 * Controller class
 * getters for getting coordinates of specific trominos
 * tile method for taking input for missing missing x,y and board size
 * @author Greg Kline
 *
 */

public class Controller {

	private Model queens;
	@SuppressWarnings("unused")
	private View theView;

	public static void main(String[] args) {
		new Controller();
	}

	public Controller() {
		theView = new View(this);
		queens = new Model();

	}

	@SuppressWarnings("static-access")
	public void tile(int boardSize) {
		queens.buildBoard(boardSize);
	}
	@SuppressWarnings("static-access")
	public Vector<Point> getCoords() {
		return queens.coords;
	}
	
	@SuppressWarnings("static-access")
	public Vector<Point> getQueens() {
		return queens.qCoords;
	}
	@SuppressWarnings("static-access")
	public void startQueen() {
		queens.queens();
	}

}
