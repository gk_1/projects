package nqueens;

import java.awt.Point;
import java.util.Vector;

public class Model {
	public static Vector<Point> coords = new Vector<Point>();
	public static Vector<Point> qCoords = new Vector<Point>();
	public static int board;

	/**
	 * Generate all possible coordinates based on boardSize
	 * 
	 * @param boardSize
	 *            board size, if entered 8 then you'd have an 8x8 board
	 */
	public static void buildBoard(int boardSize) {
		board = boardSize;
		for (int i = 0; i < board; i++) {
			for (int j = 0; j < board; j++) {
				coords.add(new Point(i, j));
			}
		}
	}

	/**
	 * Place queen
	 */
	public static void queens() {
		for (int i = 0; i < board; i++) {
			for (int j = 0; j < board; j++) {
				if (checkDirections(qCoords, i, j)) {
					qCoords.add(new Point(i, j));
				}
			}
		}
		// Check if the correct # of queens have been placed
		if (qCoords.size() < board) {
			// Make sure you'll still have one queen before removing another
			while (qCoords.size() > 1) {
				qCoords.remove(qCoords.size() - 1);
			}
			int x = qCoords.get(qCoords.size() - 1).x;
			int y = qCoords.get(qCoords.size() - 1).y;
			if (x < board) {
				qCoords.get(qCoords.size() - 1).setLocation(x + 1, y);
			} else {
				queens();
			}
			queens();
		}
	}

	/**
	 * Check if it's ok to place a queen at a point
	 * 
	 * @param temp
	 *            vector of all possible points
	 * @param x
	 *            where you want to place queen
	 * @param y
	 *            where you want to place queen
	 * @return whether the point is valid or not
	 */
	public static boolean checkDirections(Vector<Point> temp, int x, int y) {
		for (int i = 0; i < temp.size(); i++) {
			if (temp.get(i).x == x) {
				return false;
			}
			else if (temp.get(i).y == y) {
				return false;
			}
			else if (Math.abs(temp.get(i).x - x) == Math.abs(temp.get(i).y - y)) {
				return false;
			}
		}

		return true;

	}

}
