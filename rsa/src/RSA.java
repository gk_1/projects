import java.math.*;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

/**
 * Convert string into ascii, RSA encrypts ascii, uses private key to decrypt
 * ascii, translate ascii back to string
 * 
 * @author Greg Kline
 *
 */
public class RSA {

	public static Vector<BigInteger> pubKey = new Vector<BigInteger>();
	public static Vector<BigInteger> privKey = new Vector<BigInteger>();
	public static String output;

	/*
	 * n & phi take in p & q; e takes in phi; d takes in phi & e
	 */
	public BigInteger n, e;
	public static BigInteger nLength;
	public static BigInteger d;
	public BigInteger p;
	public BigInteger q;
	public BigInteger phi;
	public BigInteger[] hold, encr, dec;

	public static void main(String[] args) {

	}

	// Takes string, translates it to ascii
	public void setMessage(String tmessage) {
		hold = ascii(tmessage);
	}

	/**
	 * decrypt an encrypted BigInteger array based off of private key. result should
	 * be the ascii equivalent to the original message.
	 * 
	 * @param d1
	 *            private key
	 */
	public void decryption(BigInteger d1) {
		dec = decrypt(encr, d1, n);
		// dec = decrypt(encr, d, n);
	}

	/**
	 * runs the decrypted array, which is in ascii, through a method that translates
	 * ascii back to string message
	 * 
	 * @return result
	 */
	public String asciiToString() {
		return trans(dec);
	}

	/**
	 * Takes in the user set message, public key, and known mod
	 */
	public void encryption() {
		encr = encrypt(hold, e, n);
	}

	/**
	 * user inputs public key
	 * 
	 * @param e1
	 *            public key
	 */
	public void setE(BigInteger e1) {
		e = e1;
	}

	/**
	 * user inputs private key
	 * 
	 * @param d1
	 *            private key
	 */
	public void setD(BigInteger d1) {
		d = d1;
	}

	/**
	 * When called, regenerate p, q, n, phi, e, and d
	 */
	public void genKeys() {
		p = prime();
		q = prime();
		n = calcN(p, q);
		phi = calcPhi(p, q);
		e = BigInteger.valueOf(7);
		d = calcD(phi, e);
		nLength = BigInteger.valueOf(n.toString().length());
	}

	// public void quick(String temp) {
	// message = temp;
	// p = prime();
	// q = prime();
	// n = calcN(p, q);
	// phi = calcPhi(p, q);
	// e = BigInteger.valueOf(7);
	// d = calcD(phi, e);
	// hold = ascii(message);
	// encr = encrypt(hold, e, n);
	// dec = decrypt(encr, d, n);
	//
	// output = (trans(dec));
	// System.out.println(genString(encr));
	//
	// }

	/**
	 * Grab a random prime number with bitlength 70
	 * 
	 * @return large prime number
	 */
	public static BigInteger prime() {

		Random rnd = new Random();
		BigInteger test = BigInteger.probablePrime(70, rnd);
		return test;

	}

	public static BigInteger calcN(BigInteger p, BigInteger q) {
		BigInteger n = p.multiply(q);

		return n;
	}

	public static BigInteger calcPhi(BigInteger p, BigInteger q) {
		BigInteger phi = (p.subtract(BigInteger.ONE)).multiply(q.subtract(BigInteger.ONE));
		return phi;
	}

	/**
	 * calculate private key
	 * 
	 * @param phi
	 *            phi
	 * @param e
	 *            public key
	 * @return private key
	 */
	public static BigInteger calcD(BigInteger phi, BigInteger e) {
		BigInteger d = e.modInverse(phi);
		return d;
	}

	/**
	 * convert message into ascii
	 * 
	 * @param m
	 *            string message
	 * @return translation of string to ascii in form of BigInteger array
	 */
	public static BigInteger[] ascii(String m) {
		BigInteger[] ascii = new BigInteger[m.length()];
		int temp;
		for (int i = 0; i < m.length(); i++) {
			temp = (int) m.charAt(i);
			ascii[i] = BigInteger.valueOf(temp);
		}
		System.out.println("Ascii: " + ascii[0] + " " + ascii[1]);
		return ascii;
	}

	/**
	 * 
	 * @param ascii
	 *            array
	 * @return translation of ascii array to string message
	 */
	public static String trans(BigInteger[] ascii) {
		String message = "";
		for (int i = 0; i < ascii.length; i++) {
			message += Character.toString((char) ascii[i].intValue());
		}
		return message;
	}

	/**
	 * 
	 * @param ascii
	 *            array
	 * @param e
	 *            public key
	 * @param n
	 *            known mod
	 * @return BigInteger array of RSA encryption from the ascii array
	 */
	public static BigInteger[] encrypt(BigInteger[] ascii, BigInteger e, BigInteger n) {
		BigInteger[] code = new BigInteger[ascii.length];
		for (int i = 0; i < ascii.length; i++) {
			code[i] = ascii[i].modPow(e, n);
		}
		return code;
	}

	/**
	 * Convert any BigInteger array quickly into a string
	 * 
	 * @param temp
	 *            BigInteger array
	 * @return string based off BigInteger array
	 */
	public static String genString(BigInteger[] temp) {
		String message = "";
		for (int i = 0; i < temp.length; i++) {
			message += " " + temp[i].intValue();
		}
		return message;
	}
	
	public static Vector<BigInteger> genDigit(BigInteger[] temp) {
		String message = "";
		Vector<BigInteger> data = new Vector<BigInteger>();
		for (int i = 0; i < temp.length; i++) {
			message += temp[i].intValue();
		}
		for(int i =0; i < message.length(); i++) {
			data.add(BigInteger.valueOf((int)message.charAt(i)));
		}
		
		return data;
	}

	/**
	 * Decrypts an encrypted ascii array based off private key and known mod
	 * 
	 * @param ascii
	 *            encrypted ascii array
	 * @param d
	 *            private key
	 * @param n
	 *            known mod
	 * @return decryped ascii array.
	 */
	public static BigInteger[] decrypt(BigInteger[] ascii, BigInteger d, BigInteger n) {
		BigInteger[] code = new BigInteger[ascii.length];
		for (int i = 0; i < ascii.length; i++) {
			code[i] = ascii[i].modPow(d, n);
		}
		return code;
	}

}
