/**
 * Greg Kline
 * 
 * Controller for RSA encryption
 */

import java.math.BigInteger;
import java.util.Scanner;

public class Controller {
	
	private RSA rsa;
	private View theView;
	
	public static void main(String[] args) {
		new Controller();
	}
	
	public Controller()
	{
		theView = new View(this);
		rsa = new RSA();
		
	}
	
	public void genKeys() {
		rsa.genKeys();
		System.out.println(rsa.d);
		
	}
	
	
	public void setM(String message) {
		rsa.setMessage(message);
	}
	
	public BigInteger getN() {
		return rsa.n;
	}
	
	public BigInteger getPub() {
		return rsa.e;
	}
	
	public String getAscii() {
		System.out.println(rsa.d);
		System.out.println("n digits: " + rsa.n.toString().length());
		return rsa.asciiToString();
	}
	
	public String getKeys() {
		String m = "D: " + rsa.d + "\n" + "E: " + "\n" + rsa.e.toString() + "\n" + "N: " + rsa.n.toString() + "\n" + "N Bits: " + rsa.n.bitLength();
		
		return m;
	}
	
	public BigInteger[] getEncrypt() {
		System.out.println(rsa.d);
	
		rsa.encryption();
//		System.out.println(rsa.temp());
		return rsa.encr;
	}
	
	public BigInteger[] getDecrypt(BigInteger d1) {
		System.out.println(rsa.d);
		rsa.decryption(d1);
		
		return rsa.dec;
	}
	
	public String getString(BigInteger[] temp) {		
		return rsa.genString(temp);
	}
	
	

}
