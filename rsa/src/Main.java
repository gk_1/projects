

public class Main {

	private Controller myController;
	
	public static void main(String[] args) {
		new Main();

	}
	
	public Main() {
		this.setController(new Controller());
	}
	
	public void setController(Controller myController) {
		this.myController = myController;
	}
	
	public Controller getController() {
		return myController;
	}

}
