/**
 * Greg Kline
 * 
 * View for RSA encryption
 */
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigInteger;
import java.util.Scanner;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

public class View implements ActionListener {
	private JButton encrypt, decrypt, setM, getAscii;
	private Controller myController;
	String message, pubkey;
	BigInteger privkey;
	JTextArea area = new JTextArea(25,25); 
	JTextArea area2 = new JTextArea(25,25);
	JTextArea area3 = new JTextArea(25,25);

	public View(Controller controller) {
		JFrame frame = new JFrame("RSA Demo");
		myController = controller;
		
		frame.setSize(500, 500);
		frame.setLayout(new FlowLayout());
//area2.setText(myController.getAscii());
		area.setSize(150, 150);
		area.setEditable(true);
		area.setLineWrap(true);
		area.setVisible(true);

		
		area2.setSize(150, 150);
		area2.setEditable(true);
		area2.setLineWrap(true);
		area2.setVisible(true);

		setM = new JButton("Set Message");
		setM.addActionListener(this);
		
		getAscii = new JButton("Get Ascii");
		getAscii.addActionListener(this);
		
		encrypt = new JButton("Encrypt Message");
		encrypt.addActionListener(this);
		
		decrypt = new JButton("Decrypt Message");
		decrypt.addActionListener(this);
		
		frame.add(setM);
		frame.add(getAscii);
		frame.add(encrypt);
		frame.add(decrypt);
		frame.add(area);
		frame.add(area2);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		if (arg0.getSource() == setM) {
			String temp = area.getText();
			myController.setM(temp);
		}
		
		if (arg0.getSource() == getAscii) {
			area2.setText(myController.getAscii());
		}
		
		if (arg0.getSource() == encrypt) {
			myController.genKeys();
			area2.setText(myController.getString(myController.getEncrypt()));
			area.setText(myController.getKeys());
		}
		
		if (arg0.getSource() == decrypt) {
			
			String temp=JOptionPane.showInputDialog("Enter the private key ");
			privkey = new BigInteger(temp);
			area2.setText("If the key was entered correctly, you should be seeing ASCII");
			//myController.getDecrypt(privkey);
			area.setText(myController.getString(myController.getDecrypt(privkey)));
		}

	}

}
